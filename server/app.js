var express = require('express');
var app = express();
app.listen(3000);
var SerialPort = require('serialport');
const Readline = SerialPort.parsers.Readline;
const pw = 3924;
var checkState = "RSSI";
var now = "LOCK";
var GPIO = require('onoff').Gpio,
    led = new GPIO(14, 'out'),
    button = new GPIO(15, 'in', 'both');

function light(err, state) {
	if (state == 1) {
		checkState = "RSSI";
		console.log('on');
	}
	else {
		checkState = "PW";
		console.log('off');
	}
}

button.watch(light);

const { exec } = require('child_process');
// exec("./bluetooth", (error, stdout, stderr) => {
	// if (error) {
		// console.error(`exec error: ${error}`);
		// return;
	// }
	// console.log(stdout);
// });
exec("ls /dev | grep ttyACM", (error, stdout, stderr) => {
	if (error) {
		console.error(`exec error: ${error}`);
		return;
	}
	console.log(stdout);
	var port = new SerialPort("/dev/" + stdout.trim(), {
		baudRate: 115200
	});	

	port.on('open', function() {
		console.log("UART OPEN");	
	});
	port.on('error', function(err) {
		console.log('Error: ', err.message);
	});

	const parser = new Readline();
	port.pipe(parser);
	parser.on('data', (data)=>{
		if(data.toString().includes("Found ID #")){
			if(now == "LOCK"){
				console.log("=====UNLOCK!!!!!!!!!!!!======");
				now = "UNLOCK";
			}
		}
		console.log(data);
	});
	
	console.log(`stdout: ${stdout}`);
	console.log(`stderr: ${stderr}`);
});

app.all("/rssi/lock/", function(req, res){
	if(checkState == "RSSI"){
		if(now == "LOCK"){
			led.writeSync(0);
			res.send('{"look" : false, "now" : "' + now + '"}');
		}else{
			led.writeSync(1);
			now = "LOCK";
			res.send('{"look" : true, "now" : "' + now + '"}');
		}
	}
});

app.all("/lock/change", function(req, res){
	if(checkState == "RSSI"){
		checkState = "RSSI";
	}else
		checkState = "PW";
});

app.all("/rssi/unlock", function(req, res){
	if(checkState == "RSSI"){
		if(now == "LOCK"){
			now = "UNLOCK";
			led.writeSync(0);
			res.send('{"look" : false, "now" : "' + now + '"}');
		}else{
			led.writeSync(1);
			res.send('{"look" : true, "now" : "' + now + '"}');
		}
	}
});

app.all("/lock/", function(req, res){
	if(checkState != "RSSI"){
		if(now == "LOCK"){
			led.writeSync(0);
			res.send('{"look" : false, "now" : "' + now + '"}');
		}else{
			led.writeSync(1);
			now = "LOCK";
			res.send('{"look" : true, "now" : "' + now + '"}');
		}
	}
});

app.all("/unlock/*", function(req, res){
	if(checkState != "RSSI"){
		const input = Number(req.path.substring(req.path.lastIndexOf("/") + 1));
		console.log(input);
		if(input == pw && now == "LOCK"){
			now = "UNLOCK";
			led.writeSync(0);
			res.send('{"look" : false, "now" : "' + now + '"}');
		}else{
			led.writeSync(1);
			res.send('{"look" : true, "now" : "' + now + '"}');
		}
	}
});

app.get('/status', function(req, res){
	res.send('{"lock" : ' + (now=="LOCK") + ', "now" : "' + now + '", "state" : "' + checkState + '"}');
});

app.get('/signal', function(req, res){
	res.send("I'm Alive!!!");
});

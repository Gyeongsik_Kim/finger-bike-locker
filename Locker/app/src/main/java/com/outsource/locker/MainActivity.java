package com.outsource.locker;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.outsource.locker.utils.GetDataFromURL;

import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    private final String TAG = MainActivity.class.getName();
    private BluetoothAdapter mBluetoothAdapter;
    private View infoArea;
    private enum STATE {ERORR, OPEN, LOCKED};
    private STATE nowLockerState;
    private TextView infoText;
    private Thread loockerStatusThread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("자전거 자물쇠");
        infoArea = findViewById(R.id.infoArea);
        infoText = findViewById(R.id.infoText);
        permission();
        infoArea.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                switch (nowLockerState){
                    case ERORR:
                        Toast.makeText(MainActivity.this, "자물쇠에 연결할 수 없습니다", Toast.LENGTH_SHORT).show();
                        break;
                    case OPEN: {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);     // 여기서 this는 Activity의 this
                        builder.setTitle("잠금 설정")        // 제목 설정
                                .setMessage("자물쇠를 잠그시겠습니까?")        // 메세지 설정
                                .setCancelable(false)        // 뒤로 버튼 클릭시 취소 가능 설정
                                .setPositiveButton("확인", new DialogInterface.OnClickListener(){
                                    // 확인 버튼 클릭시 설정
                                    public void onClick(DialogInterface dialog, int whichButton){
                                        try {
                                            JSONObject json = new GetDataFromURL("http://192.168.0.8:3000/lock/").executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                                            if(json != null) {
                                                boolean state = json.getBoolean("look");
                                                Toast.makeText(MainActivity.this, (!state) ? "잠금 실패" : "잠금 완료", Toast.LENGTH_SHORT).show();
                                            }else{
                                                Toast.makeText(MainActivity.this, "잠금 요청 실패", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(MainActivity.this, "잠금 요청 실패", Toast.LENGTH_SHORT).show();
                                            Log.e("LOCK", e.getMessage());
                                        }
                                    }
                                })
                                .setNegativeButton("취소", new DialogInterface.OnClickListener(){
                                    // 취소 버튼 클릭시 설정
                                    public void onClick(DialogInterface dialog, int whichButton){
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog dialog = builder.create();    // 알림창 객체 생성
                        dialog.show();    // 알림창 띄우기
                        break;
                    }
                    case LOCKED: {
                        final EditText editText = new EditText(MainActivity.this);
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);     // 여기서 this는 Activity의 this
                        builder.setTitle("잠금 설정")        // 제목 설정
                                .setMessage("자물쇠를 여시겠습니까?")        // 메세지 설정
                                .setView(editText)
                                .setCancelable(false)        // 뒤로 버튼 클릭시 취소 가능 설정
                                .setPositiveButton("확인", new DialogInterface.OnClickListener(){
                                    // 확인 버튼 클릭시 설정
                                    public void onClick(DialogInterface dialog, int whichButton){
                                        try {
                                            JSONObject json = new GetDataFromURL("http://192.168.0.8:3000/unlock/" + editText.getText().toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                                            if(json != null) {
                                                boolean state = json.getBoolean("look");
                                                Toast.makeText(MainActivity.this, state ? "잠금 해제 실패" : "잠금 해제 완료", Toast.LENGTH_SHORT).show();
                                            }else{
                                                Toast.makeText(MainActivity.this, "잠금 해제 요청 실패", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(MainActivity.this, "잠금 해제 요청 실패", Toast.LENGTH_SHORT).show();
                                            Log.e("LOCK", e.getMessage());
                                        }
                                    }
                                })
                                .setNegativeButton("취소", new DialogInterface.OnClickListener(){
                                    // 취소 버튼 클릭시 설정
                                    public void onClick(DialogInterface dialog, int whichButton){
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog dialog = builder.create();    // 알림창 객체 생성
                        dialog.show();    // 알림창 띄우기
                        break;
                    }
                }
                return false;
            }
        });
        loockerStatusThread = new Thread(){
            private final String ip = "http://192.168.0.8:3000/status";
            @Override
            public void run(){
                while(true){
                    try{
                        JSONObject json = new GetDataFromURL(ip).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                        if(json != null) {
                            boolean state = json.getBoolean("look");
                            nowLockerState = state ? STATE.LOCKED : STATE.OPEN;
                            infoText.setText(String.valueOf(state));
                        }else {
                            nowLockerState = STATE.ERORR;
                            infoText.setText("ERROR");
                           }
                        Thread.sleep(100);
                    }catch (Exception e){
                        Log.e("STATUS_THREAD", e.getMessage());
                    }
                }
            }
        };
        loockerStatusThread.start();
    }

    private void adapter(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Log.e("Bluetooth", "Get Adapter fail...");
            finish();
        }else{
            if (!mBluetoothAdapter.isEnabled()) {
                Log.e("Bluetooth", "ENABLE Request!");
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 0);
            }else{
                Log.i("Bluetooth", "Enable!");
                scan();
            }
        }
    }

    private void permission(){
        String[] temp = {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        ActivityCompat.requestPermissions(
                this, temp,
                0);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    adapter();
                } else {
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0 :
                if (resultCode == RESULT_OK) {
                    Toast.makeText(MainActivity.this, "ENABLE", Toast.LENGTH_SHORT).show();
                    scan();
                }else{
                    finish();
                }
                break;
        }
    }

    private BluetoothAdapter.LeScanCallback callback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            Log.i("BLUETOOTH", bluetoothDevice.getAddress());
            Log.i("BLUETOOTH", String.valueOf(i));
            if(bluetoothDevice.getAddress().equalsIgnoreCase("26:47:FF:DC:CE:6C")) {

            }
        }
    };

    private void scan(){
        mBluetoothAdapter.startLeScan(callback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.outsource.locker.utils;

import android.os.AsyncTask;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by 9274a on 2017-11-03.
 */

//URL 에서 데이터를 가져오는 클래스
public class GetDataFromURL  extends AsyncTask<Void, Void, JSONObject>{
    private final String TAG = GetDataFromURL.class.getName();
    private final String url;
	
	//생성자, URL 초기화
    public GetDataFromURL(String url) {
        this.url = url;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        try{
			//URL과의 HTTP connection 생성
            URL Url = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
			//URL과의 input stream을 가져옴
            InputStream is = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
			//URL에서 한줄씩 읽어와서 StringBuilder에 추가
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            line = sb.toString();
			//URL과 연결된 커넥션을 제거
            connection.disconnect();
			//inputstream 제거
            is.close();
            sb.delete(0, sb.length());
			
			//읽어온 데이터를 JSON Object로 만들어서 반환
            return new JSONObject(line);
        } catch (Exception e) {
            return null;
        }
    }

}
